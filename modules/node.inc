<?php

/**
 * Implementation of hook_filter_macros().
 */
function node_filter_macros($op, $tag) {
  switch ($op) {
    case 'list':
      return array('node-title', 'node-teaser', 'node-body', 'node-nid', 'node-created', 'node-changed', 'node-url');
      break;
    case 'info':
      if ($tag['tag'] == 'node-url') {
        return array('style' => 'multi-balance');
      }
      return array('style' => 'single');
      break;
    case 'process':
      if (!empty($tag['context']['nid']) && ($node = node_load($tag['context']['nid']))) {
        switch ($tag['tag']) {
          case 'node-title':
            return check_plain($node->title);
            break;
          case 'node-teaser':
            if ($tag['context']['teaser']) {
              return '';
            }
            return check_markup($node->teaser, $node->format, FALSE);
            break;
          case 'node-body':
            if ($tag['context']['body']) {
              return '';
            }
            return check_markup($node->body, $node->format, FALSE);
            break;
          case 'node-nid':
            return $node->nid;
            break;
          case 'node-created':
            return !empty($tag['settings'][0][0]) ? format_date($node->created, 'custom', $tag['settings'][0][0]) : format_date($node->created, 'medium');
            break;
          case 'node-changed':
            return !empty($tag['settings'][0][0]) ? format_date($node->changed, 'custom', $tag['settings'][0][0]) : format_date($node->changed, 'medium');
            break;
          case 'node-url':
            $text = '<a href="'. check_url(url('node/'. $node->nid, array('absolute' => true))) .'"';
            if (!empty($settings[0]['class'])) {
              $text .= drupal_attributes(array('class' => $settings[0]['class']));
            }
            $text .= '>'. $tag['content'] .'</a>';
            return $text;
            break;
        }
      }
      break;
  }
}
