Filter Macros
-------------

This module allows macro tags of almost every format. The processing of the
macros happens according to the additional modules that are made available
through using the hook_filter_macros.

General format for using the hook.

/**
 * Implementation of hook_filter_macros().
 */
function example_filter_macros($op, $tag) {
  switch ($op) {
    // lists the macro tags this module handles.
    case 'list':
      return array('example');

    // process the macro tags that are found in the text.
    case 'process':
      return '<p>'. t('This converts the tag [example] into this text.') .'</p>';
  }
}

More complex example of using the hook.

/**
 * Implementation of hook_filter_macros().
 */
function example_filter_macros($op, $tag) {
  switch ($op) {
    // lists the macro tags this module handles.
    case 'list':
      return array('example', 'settings');

    // process the macro tags that are found in the text.
    case 'process':
      switch ($tag['tag']) {
        case 'example':
          return '<p>'. t('This converts the tag [example] into this text.') .'</p>';

        case 'settings':
          // uses the settings from the first tier, i.e. that which comes after a ':'.
          if (!empty($tag['settings'][0])) {
            $output = '<ul>';
            foreach ($tag['settings'][0] as $key => $value) {
              $output .= '<li>'. $key .' = '. $value .'</li>';
            }
            $output .= '</ul>';
          }
          break;
      }
  }
}


Examples
--------

Basic tags:
[audio], [video], [image]

Simple setting:
[audio:no-player], [video:width=200;height=200;], [image:width=200;height=200]

Complex settings:
[audio:no-player:high-quality:alternative],
[video:width=200;height=200;:jw_flv_player],
[image:width=200;height=200:caption="Help":layout="float-right"]

From a programmatic standpoint, the settings are defines as an array of arrays. The first tier, is encompassed by the
first ':' that takes place. The individual elements of the array are seperated by the ';' character. The array generates
those elements without a key a numeric key, as simply adding an element to an array will do.

