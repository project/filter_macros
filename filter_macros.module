<?php

/**
 *  List of defines for searching for the filter macros.
 */
define('FILTER_MACRO_NUMBER', '(?:\-|)(?:\d+)(?:\.\d+|)');
define('FILTER_MACRO_STRING', '"(?:\\\\"|[^"\n]|)+"');
define('FILTER_MACRO_WORD', '[\w\-\_]+');
define('FILTER_MACRO_OPTION', '(?:'. FILTER_MACRO_NUMBER .'|'. FILTER_MACRO_STRING .'|'. FILTER_MACRO_WORD .')');
define('FILTER_MACRO_ARRAY_OPTION', '(?:'. FILTER_MACRO_OPTION .')=' . FILTER_MACRO_OPTION );
define('FILTER_MACRO_ARRAY_ITEM', '(?:'. FILTER_MACRO_ARRAY_OPTION .'|'. FILTER_MACRO_OPTION .'|)');
define('FILTER_MACRO_ARRAY', '(?:(?:'. FILTER_MACRO_ARRAY_ITEM .';)*(?:'. FILTER_MACRO_ARRAY_ITEM .'))');
define('FILTER_MACRO_TAG', '(?:(?:\/|)[\w\d\-\_]+)');
define('FILTER_MACRO', '\[('. FILTER_MACRO_TAG .')((?::'. FILTER_MACRO_ARRAY .')*|)\]');

/**
 * Implementation of hook_init().
 */
function filter_macros_init() {
  $path = drupal_get_path('module', 'filter_macros');

  // Load module support additions
  $files = drupal_system_listing('.*\.inc$', $path .'/modules', 'name', 0);
  foreach ($files as $module => $file) {
    if (module_exists($module)) {
      require_once($file->filename);
    }
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function filter_macros_nodeapi(&$node, $op, $teaser) {
  static $nodes = array();
  switch ($op) {
    case 'load':
      // convert all tags in teaser to context of teaser.
      $string = filter_macros_process($node->teaser, array('context' => 'body', 'nid' => $node->nid));
      if ($string != $node->teaser) {
        $nodes[$node->nid]['teaser'] = $node->teaser;
        $node->teaser = $string;
      }

      // convert all tags in body to context of body
      $string = filter_macros_process($node->body, array('context' => 'body', 'nid' => $node->nid));
      if ($string != $node->body) {
        $nodes[$node->nid]['body'] = $node->body;
        $node->body = $string;
      }
      break;

    case 'prepare':
      // restore for editing purposes
      if (array_key_exists($node->nid, $nodes)) {
        if (!empty($nodes[$node->nid]['teaser'])) {
          $node->teaser = $nodes[$node->nid]['teaser'];
        }
        if (!empty($nodes[$node->nid]['body'])) {
          $node->body = $nodes[$node->nid]['body'];
        }
      }
      break;

    case 'view':
      // make sure that viewing has any filter_macros processed.
      $node->content['body']['#value'] = filter_macros_process($node->content['body']['#value']);
      break;
  }
}

/**
 * Implementation of hook_filter().
 */
function filter_macros_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      // only the one filter
      return array(0 => t('Filter Macros'));

    // never cache the filter macros
    case 'no cache':
      return true;

    case 'description':
      return t("Converts macros in filters to HTML or text.");

    // simply return the text, with any context added
    case 'prepare':
      return filter_macros_process($text, array());

    // processes global filter versions
    case 'process':
      return filter_macros_process($text, null);
  }
}

/**
 * Implementation of hook_filter_tips().
 */
function filter_macros_filter_tips($delta, $format, $long = false) {
  return !$long ?
    t('Provides a general macro filter.') :
    t('This macro filter can generically allow tags of the style [video], [video:widescreen], and a variety of other formats to display what the modules define for them.');
}

/**
 * Lists all the macros currently available for filter_macros.
 */
function filter_macros_list() {
  return filter_macros_invoke('list');
}

/**
 * Gets information about how a tag functions.
 */
function filter_macros_info($tag) {
  static $cache = array();
  if (!array_key_exists($tag['tag'], $cache)) {
    $cache[$tag['tag']] = filter_macros_invoke('info', $tag);
  }
  return $cache[$tag['tag']];
}

/**
 * Gets the description for the tag, generally useful for displaying the
 * various options allowable for this tag.
 */
function filter_macros_description($tag) {
  $description = filter_macros_invoke('description', $tag);
  return $description ? $description : '';
}


/**
 *  Produce the appropriate configure form for a filter tag to be generated
 */
function filter_macros_form_configure($tag, $settings, $context, $full) {
  return filter_macros_invoke('configure', $tag, $settings, $context, $full);
}

/**
 * Validates a configure form generated from above.
 * Returns the errors generated by the tag, in the form.
 * array('field_name' => 'field_name', 'error' => 'Error message'),
 */
function filter_macros_form_validate($tag, $settings, $context, $full) {
  return filter_macros_invoke('validate', $tag, $settings, $context, $full);
}

/**
 * Get the context from settings, if there exists one
 */
function _filter_macros_context(&$settings) {
  $context = null;
  if (count($settings) > 0) {
    $item = count($settings) - 1;
    if (!empty($settings[$item]['context'])) {
      $context = $settings[$item];
      unset($settings[$item]);
    }
  }
  return $context;
}

/**
 * Processes the current tag.
 */
function _filter_process_tag(&$balance, &$current, $context, $output) {
  $pos = $current['pos'];
  $full = $current['full'];
  $full['content'] = substr($output, $pos);
  $full['close'] = false;
  if (!is_null($context)) {
    if (in_array($full['tag'], filter_macros_list()) && !empty($context)) {
      $full['settings'][] = $context;
    }
    // always balanced tags
    $output = substr($output, 0, $pos) . filter_macros_tag($full['tag'], $full['settings']) . $full['content'] .'[/'. $full['tag'] .']';
  }
  else {
    $full['context'] = _filter_macros_context($full['settings']);
    $output = substr($output, 0, $pos) . filter_macros_invoke('process', $full);
  }
  $current = array_pop($balance);
  return $output;
}

/**
 * Check for closing tag, and close if necessary
 */
function _filter_process_close_tag(&$balance, &$current, $tag, $context, $output) {
  // current no-tag close
  if ($current['tag'] == $tag['tag']) {
    // handle the tag being closed
    $output = _filter_process_tag($balance, $current, $context, $output);
  }
  else {
    // find if there is something to back step towards
    foreach (array_reverse($balance) as $key => $stack) {
      if ($stack['tag'] == $tag['tag']) {
        // relies on key being numeric
        while (count($balance) > $key) {
          $output = _filter_process_tag($balance, $current, $context, $output);
        }
        break;
      }
    }
  }
  return $output;
}

/**
 * Processes text from a filter.
 *
 * style = nested, single-balance, multi-balance, single, no-tag
 * keep-whitespace = false (won't keep whitespace if defined)
 *
 * - a "no-tag" completely ignores all tags until the closing tag is reached
 * - a "single" tag does not require another tag to follow it of closed type, any closed tags of this are dropped completely.
 * - a "single-balance" MUST be followed by another of it's type
 * - a "multiple-balance" can be followed by other multiple-balance of other types,
 *   or by single balance. another multiple-balance of the same tag, forces a finish to that multiple-balance, and
 *   a replacement by that multiple-balance
 * - a "nested" allows multiple balances of the same type.
 */
function filter_macros_process($text, $context = null) {
  // ignore empty
  if (!$text) {
    return $text;
  }

  $balance = array();
  $current = false;

  $last = 0;
  $output = '';
  $tags = filter_macros_find($text);
  foreach ($tags as $tag) {
    // keep the current position within the output
    $curpos = strlen($output);

    // check for keeping whitespace
    $substr = substr($text, $last, $tag['pos'] - $last);
    if (isset($current['keep-whitespace']) && !$current['keep-whitespace']) {
      $substr = trim($substr);
    }
    $output .= $substr;

    // now get the information for the current tag
    $info = filter_macros_info($tag);

    // default to ignore
    $info['style'] = empty($info['style']) ? 'ignore' : $info['style'];

    // handle closing scenarios for each possible style
    $continue = false;
    switch ($current['style']) {
      case 'no-tag':
        // no-tag requires a matching end
        if ($current['tag'] != $tag['tag']) {
          $last = $tag['pos'];
          $continue = true;
        }
        // handle nested no-tags
        elseif (!$tag['close']) {
          // process the tag
          $output = _filter_process_tag($balance, $current, $context, $output);

          // readjust curpos since we've changed the output
          $curpos = strlen($output);
        }
        break;
      case 'single-balance':
        // grab the content from just after this tag
        $current['pos'] = $curpos;
        $output = _filter_process_tag($balance, $current, $context, $output);

        // readjust curpos since we've changed the output
        $curpos = strlen($output);

        // if we have a successful end of single-balance it's considered processed, otherwise, it's new tag
        if ($current['tag'] == $tag['tag'] && $tag['close']) {
          $last = $tag['pos'] + strlen($tag['full']);
          $continue = true;
        }
        break;
      case 'multi-balance':
        // handle multiple-balance with a new open tag, when current
        if ($current['tag'] == $tag['tag'] && !$tag['close']) {
          $output = _filter_process_tag($balance, $current, $context, $output);
          // readjust curpos since we've changed the output
          $curpos = strlen($output);
        }
        break;
    }
    // ignore the tag processing
    if ($continue) {
      continue;
    }

    // we only backstep if told
    switch ($info['style']) {
      case 'no-tag':
        // make sure we're at the beginning of the tag output
        $curpos = !$tag['close'] ? strlen($output) : $curpos;
      case 'single-balance':
      case 'multi-balance':
      case 'nested':
        // close up to tag if we're doing a multiple-balance of the same tag
        if ($info['style'] == 'multi-balance') {
          $output = _filter_process_close_tag($balance, $current, $tag, $context, $output);
          $curpos = strlen($output);
        }
        // if open no-tags, then push the current onto the stack.
        if (!$tag['close']) {
          $curpos = $info['style'] == 'nested' ? strlen($output) : $curpos;
          // handle the tag being opened
          array_push($balance, $current);
          $current = array_merge($info, array('tag' => $tag['tag'], 'pos' => $curpos, 'full' => $tag));
        }
        // single balance doesn't look to be finished
        elseif (!in_array($info['style'], array('single-balance', 'multi-balance'))) {
          $output = _filter_process_close_tag($balance, $current, $tag, $context, $output);
        }
        break;
      // process the single since there is nothing special about it.
      case 'single':
        // ignore close tags with singles
        if (!$tag['close']) {
          if (!is_null($context)) {
            if (in_array($tag['tag'], filter_macros_list()) && !empty($context)) {
              $tag['settings'][] = $context;
            }
            $output .= filter_macros_tag($tag['tag'], $tag['settings']);
          }
          else {
            $tag['context'] = _filter_macros_context($tag['settings']);
            $output .= filter_macros_invoke('process', $tag);
          }
        }
        break;
      // output the tag, since it doesn't have any handler
      default:
        $output .= $tag['full'];
        break;
    }
    // should always move the last pointer to after the tag.
    $last = $tag['pos'] + strlen($tag['full']);
  }
  // process any unfinished unbalanced tags
  while ($current || count($balance) > 0) {
    $output = _filter_process_tag($balance, $current, $context, $output);
  }
  $output .= substr($text, $last);

  return $output;
}

/**
 * Returns a list of all the valid tags located within a text.
 */
function filter_macros_find($text) {
  $tags = array();
  if (preg_match_all('/'. FILTER_MACRO .'/', $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE)) {
    // check each tag
    foreach ($matches as $match) {
      $pos = $match[0][1];
      $settings = array();
      // check all additional settings groups.
      if (preg_match_all('/:'. FILTER_MACRO_ARRAY .'/', $match[2][0], $array, PREG_SET_ORDER)) {
        foreach ($array as $item) {
          $part = array();
          // the individual items
          $values = preg_split('/('. FILTER_MACRO_ARRAY_ITEM .')/', ltrim($item[0], ':'), -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

          // process values
          foreach ($values as $value) {
            // skip delimiters
            if ($value == ';') {
              continue;
            }

            // split each value
            $value = preg_split('/('. FILTER_MACRO_OPTION .')/', $value, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

            // reprocess any string values into proper format
            foreach ($value as $k => $v) {
              if (is_string($v) && $v{0} == '"') {
                $v = substr($v, 1, strlen($v)-2);
                $value[$k] = str_replace('\\"', '"', $v);
              }
            }

            // this is an indexed array item
            if (count($value) == 3) {
              $part[$value[0]] = $value[2];
            }
            // this is an unindexed array item
            else {
              $part[] = $value[0];
            }
          }

          $settings[] = $part;
        }
      }

      // get tag and whether it is closed or not
      $tag = $match[1][0];
      $close = substr($tag, 0, 1) == '/';
      $tag = $close ? substr($tag, 1) : $tag;
      $tags[] = array(
        'full' => $match[0][0],
        'tag' => $tag,
        'settings' => $settings,
        'pos' => $pos,
        'content' => '',
        'close' => $close,
      );
    }
  }

  return $tags;
}

/**
 *  Generates a tag based on the tag, settings and profile.
 */
function filter_macros_tag($tag, $settings = array()) {
  $output = '';
  if (!is_string($tag) && !is_array($settings)) {
    return $output;
  }
  $output = '['. $tag;
  if (!empty($settings)) {
    $flat = true;
    $soutput = array();
    // handle settings groups
    $group_count = 0;
    foreach ($settings as $group_key => $group) {
      // group of settings
      if (is_array($group)) {
        $count = 0;
        $list = array();
        foreach ($group as $key => $value) {
          // convert the key and value to something that matches something necessary
          foreach (array('key', 'value') as $item) {
            if (!preg_match('/^('. FILTER_MACRO_WORD .'|'. FILTER_MACRO_NUMBER .')$/D', $$item)) {
              $$item = '"'. str_replace('"', '\\"', $$item) .'"';
            }
          }
          // allow for automatically given keys
          $k = $key .'=';
          if (is_numeric($key) && $key > -1) {
            $count = ($key == $count) ? $count + 1 : -1;
            $k = $count < 0 ? $k : '';
          }
          $list[] = $k . $value;
        }

        $flat = false;
        $soutput[] = implode(';', $list);
      }
      // individual settings
      else {
        foreach (array('group_key', 'group') as $item) {
          if (!preg_match('/^('. FILTER_MACRO_WORD .'|'. FILTER_MACRO_NUMBER .')$/D', $$item)) {
            $$item = '"'. str_replace('"', '\\"', $$item) .'"';
          }
        }
        $k = $group_key .'=';
        if (is_numeric($group_key) && $group_key > -1) {
          $group_count = ($group_key == $group_count) ? $group_count + 1 : -1;
          $k = $group_count < 0 ? $k : '';
        }
        $soutput[] = $k . $group;
      }
    }
    if (count($soutput) > 0) {
      $output .= ':'. implode($flat ? ';' : ':', $soutput);
    }
  }
  $output .= ']';

  return $output;
}

/**
 *  Invoke hook for filter_macros.
 */
function filter_macros_invoke($op, $tag = null) {
  static $cache;

  // generate the list of filter handlers that it can deal with
  if (!isset($cache)) {
    $cache = array();
    foreach (module_implements('filter_macros') as $module) {
      // handle those which are generic
      $list = module_invoke($module, 'filter_macros', 'list', array());
      if (is_array($list)) {
        foreach ($list as $item) {
          if ($item && is_string($item)) {
            $cache[$item][] = $module;
          }
        }
      }
    }
  }

  switch ($op) {
    case 'list':
      return array_keys($cache);
    default:
      // apply to all filter_macros
      if (is_null($tag)) {
        return implode('', module_invoke_all('filter_macros', $op, null));
      }
      elseif (!empty($tag['tag']) && array_key_exists($tag['tag'], $cache)) {
        $result = null;
        foreach ($cache[$tag['tag']] as $module) {
          $func = $module .'_filter_macros';
          if ($result = $func($op, $tag)) {
            return $result;
          }
        }
        return $result;
      }
      break;
  }
}
